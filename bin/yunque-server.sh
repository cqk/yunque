#!/bin/bash

# Copyright 1999-2018 Alibaba Group Holding Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

[ ! -e "$JAVA_HOME/bin/java" ] && JAVA_HOME=$HOME/jdk/java
[ ! -e "$JAVA_HOME/bin/java" ] && JAVA_HOME=/usr/java
[ ! -e "$JAVA_HOME/bin/java" ] && JAVA_HOME=/opt/taobao/java
[ ! -e "$JAVA_HOME/bin/java" ] && unset JAVA_HOME

if [ -z "$JAVA_HOME" ]; then
  if $darwin; then

    if [ -x '/usr/libexec/java_home' ] ; then
      export JAVA_HOME=`/usr/libexec/java_home`

    elif [ -d "/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home" ]; then
      export JAVA_HOME="/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home"
    fi
  else
    JAVA_PATH=`dirname $(readlink -f $(which javac))`
    if [ "x$JAVA_PATH" != "x" ]; then
      export JAVA_HOME=`dirname $JAVA_PATH 2>/dev/null`
    fi
  fi
  if [ -z "$JAVA_HOME" ]; then
        error_exit "Please set the JAVA_HOME variable in your environment, We need java(x64)! jdk8 or later is better!"
  fi
fi

export SERVER="com.larkmidtable.yunque.YunQueEngineServer"

export JAVA_HOME

export JAVA="$JAVA_HOME/bin/java"

export BASE_DIR=`cd $(dirname $0)/..; pwd`

export CONF_DIR=${BASE_DIR}/conf

# 读取JVM配置
while read line
do
    if [[ ! $line == \#* ]] && [ -n "$line" ]; then
        JAVA_OPT="$JAVA_OPT $line"
    fi
done < ${CONF_DIR}/jvm_options

echo "$JAVA $JAVA_OPT_EXT_FIX ${JAVA_OPT}"



# 启动日志
if [ ! -d "${BASE_DIR}/logs" ]; then
  mkdir -p ${BASE_DIR}/logs
fi

if [ ! -f "${BASE_DIR}/logs/server.out" ]; then
  touch "${BASE_DIR}/logs/server.out"
fi


# pid
if [ ! -d "${BASE_DIR}/pid/" ]; then
  mkdir -p ${BASE_DIR}/pid/
fi

PIDFILE=${BASE_DIR}/server.pid

# 启动云雀服务
start_server() {
  if [ -f "$PIDFILE" ] && kill -0 $(cat "$PIDFILE"); then
    echo "server is already running..."
    exit 1
  fi

  #启动
  ${JAVA} ${JAVA_OPT} -cp "${BASE_DIR}/lib/*" ${SERVER} -port $1 >> ${BASE_DIR}/logs/server.out 2>&1 &

  echo $! > $PIDFILE

  echo "start up success...."
}


stop_server(){
  PID=$(cat $PIDFILE)
  echo "$PID server stoping..."
  kill -9 $PID
  rm -rf $PIDFILE
}

restart_server(){
  stop_server
  sleep 2
  start_server $2
}

case $1 in
	start)
		start_server $2
		;;
	stop)
		stop_server
		;;
  restart)
		restart_server $2
		;;
	*)
		echo "Usage: $0 {start|stop|restart}"
	;;
esac