package www.larkmidtable.com.element;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Types;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import www.larkmidtable.com.exception.YunQueException;
import www.larkmidtable.com.util.DateUtil;

/**
 *
 * @author fei
 * @date 2023-07-12
 * 
 **/
public class Transformer {
	private static Logger logger = LoggerFactory.getLogger(Transformer.class);

	public static Column buildColumn(ResultSet rs, ResultSetMetaData metaData, int columnIndex) {
		Column column = null;
		try {
			switch (metaData.getColumnType(columnIndex)) {

			case Types.CHAR:
			case Types.NCHAR:
			case Types.VARCHAR:
			case Types.LONGVARCHAR:
			case Types.NVARCHAR:
			case Types.LONGNVARCHAR:
				column = new Column().setRawData(rs.getString(columnIndex)).setType(Column.Type.STRING);
				break;

			case Types.CLOB:
			case Types.NCLOB:
				column = new Column().setRawData(rs.getString(columnIndex)).setType(Column.Type.STRING);
				break;

			case Types.SMALLINT:
			case Types.TINYINT:
			case Types.INTEGER:
			case Types.BIGINT:
				column = new Column().setRawData(rs.getLong(columnIndex)).setType(Column.Type.LONG);
				break;

			case Types.NUMERIC:
			case Types.DECIMAL:
			case Types.FLOAT:
			case Types.REAL:
			case Types.DOUBLE:
				column = new Column().setRawData(rs.getString(columnIndex)).setType(Column.Type.DOUBLE);
				break;

			case Types.TIME:
				column = new Column().setRawData(rs.getTime(columnIndex)).setType(Column.Type.TIME);
				break;

			case Types.DATE:
				if (metaData.getColumnTypeName(columnIndex).equalsIgnoreCase("year")) {
					column = new Column().setRawData(rs.getInt(columnIndex)).setType(Column.Type.LONG);
				} else {
					column = new Column().setRawData(rs.getDate(columnIndex)).setType(Column.Type.DATE);
				}
				break;

			case Types.TIMESTAMP:
				column = new Column().setRawData(rs.getTimestamp(columnIndex)).setType(Column.Type.DATETIME);
				break;

			case Types.BINARY:
			case Types.VARBINARY:
			case Types.BLOB:
			case Types.LONGVARBINARY:
				column = new Column().setRawData(rs.getBytes(columnIndex)).setType(Column.Type.BYTES);
				break;

			case Types.BOOLEAN:
			case Types.BIT:
				column = new Column().setRawData(rs.getBoolean(columnIndex)).setType(Column.Type.BOOL);
				break;

			case Types.NULL:
				String stringData = null;
				if (rs.getObject(columnIndex) != null) {
					stringData = rs.getObject(columnIndex).toString();
				}
				column = new Column().setRawData(stringData).setType(Column.Type.STRING);
				break;

			default:
				throw new YunQueException(String.format(
						"您的配置文件中的列配置信息有误，不支持读取这种字段类型. 字段名:[%s], 字段名称:[%s], 字段Java类型:[%s]。请尝试使用数据库函数将其转换yunque支持的类型或者不同步该字段!",
						metaData.getColumnName(columnIndex), metaData.getColumnType(columnIndex),
						metaData.getColumnClassName(columnIndex)));
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return column;
	}

	/**
	 * 封装特殊类型的字段
	 * 
	 * @param sourceColumn
	 * @return
	 */
	public static Column reverseBuildColumn(Column sourceColumn) {
		Column column = null;
		try {
			switch (sourceColumn.getType()) {

			case DATETIME:
				// 处理日期类型
				sourceColumn
						.setRawData(DateUtil.formatDateTime(Long.valueOf(String.valueOf(sourceColumn.getRawData()))));
				column = sourceColumn;
				break;

			default:
				column = sourceColumn;
				break;
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return column;
	}

}
