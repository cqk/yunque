package www.larkmidtable.com;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import www.larkmidtable.com.concurrent.TaskParams;

import java.sql.Connection;


/**
 * @Description: [DorisReaderParams]
 * @Title:
 * @Author: tony
 * @Date: 2023-07-02
 */
@Deprecated
@Getter
@Setter
@AllArgsConstructor
public class DorisReaderParams extends TaskParams {

    private Connection connection;
    private String splitSql;
}
