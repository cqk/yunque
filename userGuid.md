# Quick Start

### 1.下载云雀源码

```
$ git clone https://gitee.com/LarkMidTable/yunque.git
```

### 2.通过maven打包

```
$ cd  {yunque_source_code_home}
$ mvn clean install -DskipTests
```
> 注意：如果是在windows系统编译打包的，bin下的*.sh脚本编码格式是dos，在Linux系统上运行时，需要使用 vim 文件名,然后set ff=unix 或 :set fileformat=unix改编码格式。

打包成功，日志显示如下：
```
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  03:36 min
[INFO] Finished at: 2023-05-15T14:33:42+08:00
[INFO] ------------------------------------------------------------------------

```
打包成功后的tar.gz包位于 {yunque_source_code_home}/build/yunque-1.0-SNAPSHOT.tar.gz,如下：

### 项目部署
将部署包上传至服务器

```
[root@master ~]# ls
yunque-1.0-SNAPSHOT.tar.gz

```
创建目录并解压缩：
```
[root@master ~]# mkdir yunque-client yunque-server
[root@master ~]# tar -zxvf yunque-1.0-SNAPSHOT.tar.gz
[root@master ~]# ll
总用量 293884
-rw-r--r-- 1 root root 150489007 9月  26 21:36 yunque-1.0-SNAPSHOT.tar.gz
drwxr-xr-x 5 root root        40 9月  26 21:39 yunque-client
-rw-r--r-- 1 root root  75099846 9月  26 21:18 yunque-client-1.0-SNAPSHOT.tar.gz
drwxr-xr-x 8 root root       145 9月  26 22:05 yunque-server
-rw-r--r-- 1 root root  75344487 9月  26 21:17 yunque-server-1.0-SNAPSHOT-release.tar.gz

解压客户端
[root@master ~]# tar -zxvf yunque-client-1.0-SNAPSHOT.tar.gz -C ./yunque-client
[root@master ~]# cd yunque-client
[root@master yunque-client]# ls
bin  conf  lib

解压服务端
[root@master ~]# tar -zxvf yunque-server-1.0-SNAPSHOT-release.tar.gz -C ./yunque-server
[root@master ~]# cd yunque-client
[root@master yunque-client]# ls
bin  conf  lib

```

### 3.配置示例
以mysql读写插件为例，创建配置文件 mysql2mysql.yaml 内容如下：
```
reader:
  plugin: "mysqlreader"
  url: "jdbc:mysql://localhost:3306/yunque?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Shanghai&useSSL=false"
  username: "root"
  password: "root"
  table: "lark_operate_log"
  column: "id,operate,user,address,createtime,region,osystem,browser,menuname,catalogname"
  thread: "2"

writer:
  plugin: "mysqlwriter"
  url: "jdbc:mysql://localhost:3306/yunque?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Shanghai&useSSL=false"
  username: "root"
  password: "root"
  table: "lark_operate_log_sink"
  column: "id,operate,user,address,createtime,region,osystem,browser,menuname,catalogname"
  thread: "2"

kafka:
  host: localhost:9092
  topic: hu_topic
  clientId: hu_client
  groupId: hu_group

log:
  logPath: /tmp/data/hulogs

#transformer:
#  - {"name": "dx_digest","parameter":{ "columnName":"role_key","paras":["md5", "toLowerCase"] }}


```

### 4.脚本授权并启动
启动服务端
```
[root@master yunque-server]# sh bin/yunque-server.sh 
/home/soft/jdk1.8.0_171/bin/java  
Usage: bin/yunque-server.sh {start|stop|restart}

启动服务
sh bin/yunque-server.sh start 6061

[root@master yunque-server]# sh bin/yunque-server.sh start 6061
/home/soft/jdk1.8.0_171/bin/java  
start up success....

查看启动日志
cat logs/server.out
YunQue后端服务启动，服务端口：6061启动...
```
客户端提交任务

```
查看帮助手册
[root@master yunque-client]# sh bin/yunque-client.sh -h

-h        Show this message.

args:
  -j  -- job 任务名称
  -i  -- jobId 任务ID
  -p  -- path 任务文件路径
  -f  -- fileFormat 文件类型
  -s  -- server 服務配置文件
  -d  -- 容器标识

提交任务
[root@master yunque-client]# sh bin/yunque-client.sh -j 测试 -i 1 -p conf/template/mysql2mysql.yaml -f YAML -s conf/server.yaml
-Xms512m -Xmx512m -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp/yunque/dump/yunque-client
[main] INFO com.larkmidtable.rpc.YunQueEngineClient - Hello! 欢迎使用云雀数据集成....
[main] INFO com.larkmidtable.rpc.YunQueEngineClient - 操作系统的信息如下:
操作系统名称：Linux
操作系统内核：amd64
可用的处理器数量：16
系统负载平均值：6.24

[main] INFO com.larkmidtable.rpc.YunQueEngineClient - JAVA的版本：1.8.0_171

[main] INFO com.larkmidtable.rpc.YunQueEngineClient - 核查参数的正确性....
[main] INFO com.larkmidtable.rpc.YunQueEngineClient - 核查参数的完成....
执行结果:调用成功!

```
### 5.查看服务端任务日志

```
tail -f logs/server.out
YunQue后端服务启动，服务端口：6066启动...
[pool-1-thread-1] INFO com.larkmidtable.yunque.rpc.ServerServiceImpl - 解析传递的参数....
[pool-1-thread-1] INFO com.larkmidtable.yunque.rpc.ServerServiceImpl - 作业名称 测试 ,作业ID 1 ,作业的路径 conf/template/mysql2mysql.yaml ,作业文件的格式 YAML
[pool-1-thread-1] INFO com.larkmidtable.yunque.rpc.ServerServiceImpl - 读取作业配置文件....
[pool-1-thread-1] INFO com.larkmidtable.yunque.rpc.ServerServiceImpl - 解析配置文件....
[pool-1-thread-1] INFO com.larkmidtable.yunque.rpc.ServerServiceImpl - 加载Transformer插件....
[pool-1-thread-1] INFO com.larkmidtable.yunque.rpc.ServerServiceImpl - 获取Reader和Writer....
[pool-1-thread-1] INFO com.larkmidtable.yunque.rpc.ServerServiceImpl - 进行读写任务....
[pool-1-thread-1] INFO www.larkmidtable.com.reader.AbstractDBReader - builder： SELECT id,name FROM (  select id,name from student ) t LIMIT 50000,5000
[pool-1-thread-1] INFO www.larkmidtable.com.reader.AbstractDBReader - builder： SELECT id,name FROM (  select id,name from student ) t LIMIT 55000,5000
[pool-1-thread-1] INFO www.larkmidtable.com.log.LogRecord - 线程名: [pool-1-thread-1], [mysqlreader 任务分片] 任务结束, 开始时间: [2023-09-26 22:43:21], 结束时间: [2023-09-26 22:43:21], 任务耗时 [1] ms
[pool-1-thread-1] INFO www.larkmidtable.com.log.LogRecord - 线程名: [pool-1-thread-1], [MySQLReader 初始化] 任务结束, 开始时间: [2023-09-26 22:43:21], 结束时间: [2023-09-26 22:43:22], 任务耗时 [86] ms
[pool-1-thread-1] INFO www.larkmidtable.com.log.LogRecord - 线程名: [pool-1-thread-1], [MySQLWriter 初始化] 任务结束, 开始时间: [2023-09-26 22:43:22], 结束时间: [2023-09-26 22:43:22], 任务耗时 [1] ms
[pool-1-thread-1] INFO www.larkmidtable.com.log.LogRecord - 线程名: [pool-1-thread-1], [MySQLReader 读取数据] 任务成功, 开始时间: [2023-09-26 22:43:22], 结束时间: [2023-09-26 22:43:22], 任务耗时 [16] ms
	--> 任务统计：共 0 条数据，最长任务执行耗时 0 ms
[pool-1-thread-1] INFO www.larkmidtable.com.log.LogRecord - 线程名: [pool-1-thread-1], [MySQLWriter 写入数据] 任务成功, 开始时间: [2023-09-26 22:43:22], 结束时间: [2023-09-26 22:43:22], 任务耗时 [20] ms
	--> 任务统计：共 0 条数据，最长任务执行耗时 0 ms
[Thread-1] INFO www.larkmidtable.com.util.JVMUtil - 开始销毁线程池...
[Thread-2] INFO www.larkmidtable.com.util.JVMUtil - 开始销毁线程池...
[Thread-2] INFO www.larkmidtable.com.util.JVMUtil - 成功销毁线程池....
[Thread-1] INFO www.larkmidtable.com.util.JVMUtil - 成功销毁线程池....
```