package www.larkmidtable.com.writer.sqlserverwriter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import www.larkmidtable.com.channel.Channel;
import www.larkmidtable.com.element.Column;
import www.larkmidtable.com.element.Transformer;
import www.larkmidtable.com.log.LogRecord;
import www.larkmidtable.com.util.DBType;
import www.larkmidtable.com.util.DBUtil;
import www.larkmidtable.com.writer.AbstractDBWriter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @author fei
 * @description: sqlserver 写工具
 * @date 2022-11-15
 */
//public class SqlServerWriter extends Writer {
public class SqlServerWriter extends AbstractDBWriter {
	private Connection connection;
	private PreparedStatement statement;
	private static Logger logger = LoggerFactory.getLogger(SqlServerWriter.class);

	@Override
	public void open() {
		LogRecord logRecord = LogRecord.newInstance();
        logRecord.start("SqlServer的Writer建立连接");
		try {
			connection = DBUtil.getConnection(DBType.SQLSERVER.getDriverClass(), configBean.getUrl(),
					configBean.getUsername(), configBean.getPassword());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			logRecord.end();
		}
	}

	@Override
	public void startWrite() {
		LogRecord logRecord = LogRecord.newInstance();
        logRecord.start("SqlServer开始写数据");
        
		List<String> poll = Channel.getQueue().poll();
		String[] columns = configBean.getColumn().split(",");
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < columns.length; i++) {
			sb.append("?,");
		}
		String whstr = sb.toString().substring(0, sb.toString().length() - 1);
		String sql = String.format("insert into %s(%s) values (%s)", configBean.getTable(), configBean.getColumn(),
				whstr);
		logger.info("执行SQL:{}", sql);
		try {
			connection.setAutoCommit(false);
			statement = connection.prepareStatement(sql); // 批量插入时ps对象必须放到for循环外面
			for (int i = 0; i < poll.size(); i++) {
				JSONObject record = JSONObject.parseObject(poll.get(i));

				for (int j = 1; j <= columns.length; j++) {
					Column column = Transformer.reverseBuildColumn(JSON.parseObject(String.valueOf(record.get(columns[j-1])), Column.class));
					statement.setObject(j,column.getRawData());
//					statement.setObject(j, record.get(columns[j - 1]));
				}
				statement.addBatch();
				if (i % 10000 == 0 && i > 0) {
					statement.executeBatch();
					connection.commit();
					statement.clearBatch();
				}
			}
			statement.executeBatch();
			connection.commit();
			statement.clearBatch();
		} catch (Exception e) {
			e.printStackTrace();
		}
		logRecord.end();
	}

	@Override
	public void close() {
		logger.info("SqlServer的Writter开始进行关闭连接开始....");
		DBUtil.close(statement, connection);
		logger.info("SqlServer的Writer开始进行关闭连接结束....");
	}

	@Override
	public List<AbstractDbWritTask> getDBWriteTasks() {
		return null; // TODO
	}
}
