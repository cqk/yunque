package www.larkmidtable.com;

import org.apache.commons.cli.ParseException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;

public class MySQLReaderWriterTest {
    private Connection conn = null;
    private Statement stmt = null;

    File file = null;
    String job_message = "{\n" +
            "  \"reader\": {\n" +
            "    \"plugin\": \"mysqlreader\",\n" +
            "    \"url\": \"jdbc:h2:~/test\",\n" +
            "    \"username\": \"sa\",\n" +
            "    \"password\": \"sa\",\n" +
            "    \"table\":  \"MY_USER\",\n" +
            "    \"column\": \"ID, NAME\",\n" +
            "    \"thread\": 2\n" +
            "  },\n" +
            "  \"writer\": {\n" +
            "    \"plugin\": \"mysqlwriter\",\n" +
            "    \"url\": \"jdbc:h2:~/test\",\n" +
            "    \"username\": \"sa\",\n" +
            "    \"password\": \"sa\",\n" +
            "    \"table\":  \"MY_USER_1\",\n" +
            "    \"column\": \"ID, NAME\",\n" +
            "    \"thread\": 5\n" +
            "  },\n" +
            "  \"log\": {\n" +
            "    \"logPath\": \"D:\\\\temp\"\n" +
            "  }\n" +
            "}";

    @Before
    public void before() throws SQLException, IOException {
        conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "sa");
        stmt = conn.createStatement();
        try{
            stmt.execute("DROP TABLE MY_USER");
            stmt.execute("DROP TABLE MY_USER_1");
            stmt.execute("CREATE TABLE MY_USER(ID VARCHAR(10) PRIMARY KEY,NAME VARCHAR(50))");
            stmt.execute("CREATE TABLE MY_USER_1(ID VARCHAR(10) PRIMARY KEY,NAME VARCHAR(50))");
            stmt.executeUpdate("INSERT INTO MY_USER VALUES('001','刘备')");
            stmt.executeUpdate("INSERT INTO MY_USER VALUES('002','关羽')");
            stmt.executeUpdate("INSERT INTO MY_USER VALUES('003','张飞')");
        }catch (Exception e){
            e.printStackTrace();
        }

        file = File.createTempFile("test", "json");
        if(!file.exists()){
            file.createNewFile();
        }
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.append(job_message);
        }
    }


    @After
    public void after() throws SQLException, IOException {
        try{
            file.delete();
        }finally {
            stmt.close();
            conn.close();
        }
    }

    @Test
    public void shouldTestMysqlReaderWriter() throws ParseException, SQLException {
        String absolutePath = file.getAbsolutePath();
        String[] args = new String[]{"-job",
                "test",
                "-jobId",
                "1",
                "-path",
                absolutePath,
                "-fileFormat",
                "JSON"};
        YunQueEngine.main(args);
        ResultSet rs = stmt.executeQuery("SELECT count(*) from MY_USER_1");
        while(rs.next()){
            String count = rs.getString("count(*)");
            Assert.assertTrue(count.equals("3"));
        }
    }
}
